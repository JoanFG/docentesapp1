﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DocentesApp.Vista.ControlesGenerales
{
  public  class Cargando : RelativeLayout

    {
        ActivityIndicator loading;

        public Cargando()
        {
            IsVisible = false;
            BackgroundColor = Colores.BarraNavegacion.MultiplyAlpha(0.6);
            loading = new ActivityIndicator
            {
                Color = Color.White,
                IsRunning = true,
                IsVisible = true
            };

            Children.Add(loading,
                Constraint.RelativeToParent((p)=> { return p.Width * 0.448; }),
                Constraint.RelativeToParent((p)=> { return p.Height * 0.484; }),
                Constraint.RelativeToParent((p)=> { return p.Width * 0.106; }),
                Constraint.RelativeToParent((p)=> { return p.Height * 0.059; }));
        }
    }
}
