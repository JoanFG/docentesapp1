﻿using DocentesApp.Controlador;
using DocentesApp.Interfaces;
using DocentesApp.Vista.ControlesGenerales;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DocentesApp
{
    public class Login : ContentPage
    {
        
        Image logoUtap;
        Label labelBienvenidos, OlvidarContraseña;
        Entry Documento, Codigo, Contraseña;
        TapGestureRecognizer TapOlvidarContrasena;
        Button Acceso;
        BoxView Fondo, circulo;
        Cargando loading;
        StackLayout Contenido;
        RelativeLayout contenedorLogin;

        public Login()
        {
            NavigationPage.SetHasNavigationBar(this, false);

            CrearVistas();
            AgregarVistas();
            AgregarEventos();
        }

        void CrearVistas()
        {
            loading = new Cargando();

            Fondo = new BoxView
            {
                BackgroundColor = Colores.Login
            };

            logoUtap = new Image
            {
                Source = Images.LogoUtap
            };

            labelBienvenidos = new Label
            {
                Text = "Bienvenidos",
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Center,
                TextColor = Colores.Primario,
                FontSize = Textos.Bienvenidos
            };

            Documento = new Entry
            {
                Placeholder = "Documento",
                PlaceholderColor = Colores.Primario,
                FontSize = Textos.TextoLogin
            };

            Codigo = new Entry
            {
                Placeholder = "Codigo",
                PlaceholderColor = Colores.Primario,
                FontSize = Textos.TextoLogin
            };

            Contraseña = new Entry
            {
                Placeholder = "Contraseña",
                PlaceholderColor = Colores.Primario,
                IsPassword = true,
                FontSize = Textos.TextoLogin
            };

            Acceso = new Button
            {
                Text = "Acceso App",
                BackgroundColor = Colores.Primario,
                TextColor = Colores.Secundario,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            };

            OlvidarContraseña = new Label
            {
                Text = "¿Olvido su Contraseña?",
                BackgroundColor = Color.Transparent,
                HorizontalOptions = LayoutOptions.Center,
                TextColor = Colores.Primario,
                FontSize = Textos.EnlacesLogin
            };

            circulo = new BoxView
            {
                BackgroundColor = Color.Green,
                CornerRadius = 40,
                Scale = 20
            };

            Contenido = new StackLayout();

            contenedorLogin = new RelativeLayout();  

            TapOlvidarContrasena = new TapGestureRecognizer();
            OlvidarContraseña.GestureRecognizers.Add(TapOlvidarContrasena);

            Content = Acceso;
        }



        void AgregarVistas()
        {
            contenedorLogin.Children.Add(Fondo,
                Constraint.RelativeToParent((c) => { return 0; }),                              //X
                Constraint.RelativeToParent((c) => { return 0; }),                              //Y
                Constraint.RelativeToParent((c) => { return 375; }),                            //W
                Constraint.RelativeToParent((c) => { return 667; }));                           //H 

            contenedorLogin.Children.Add(logoUtap,
                Constraint.RelativeToParent((c) => { return 20; }),                             //X
                Constraint.RelativeToParent((c) => { return 90; }),                             //Y
                Constraint.RelativeToParent((c) => { return 329; }),                            //W
                Constraint.RelativeToParent((c) => { return 102; }));                           //H 

            contenedorLogin.Children.Add(labelBienvenidos,
                Constraint.RelativeToParent((c) => { return 37.5; }),                           //X
                Constraint.RelativeToParent((c) => { return 228; }),                            //Y
                Constraint.RelativeToParent((c) => { return 300; }),                            //W
                Constraint.RelativeToParent((c) => { return 50; }));                            //H 

            Contenido.Children.Add(Documento);
            Contenido.Children.Add(Codigo);
            Contenido.Children.Add(Contraseña);

            contenedorLogin.Children.Add(Contenido,
                Constraint.RelativeToParent((c) => { return 64; }),                             //X
                Constraint.RelativeToParent((c) => { return 310; }),                            //Y
                Constraint.RelativeToParent((c) => { return 250; }),                            //W
                Constraint.RelativeToParent((c) => { return 120; }));                           //H 

            contenedorLogin.Children.Add(Acceso,
                Constraint.RelativeToParent((c) => { return 50; }),                             //X
                Constraint.RelativeToParent((c) => { return 500; }),                            //Y
                Constraint.RelativeToParent((c) => { return 250; }),                            //W
                Constraint.RelativeToParent((c) => { return 42; }));                            //H 

            contenedorLogin.Children.Add(OlvidarContraseña,
                Constraint.RelativeToParent((c) => { return 100; }),                            //X
                Constraint.RelativeToParent((c) => { return 560; }),                            //Y
                Constraint.RelativeToParent((c) => { return 161; }),                            //W
                Constraint.RelativeToParent((c) => { return 15; }));                            //H 

            contenedorLogin.Children.Add(loading,
               Constraint.RelativeToParent((c) => { return 0; }),                               //X
               Constraint.RelativeToParent((c) => { return 0; }),                               //Y
               Constraint.RelativeToParent((c) => { return c.Width; }),                         //W
               Constraint.RelativeToParent((c) => { return c.Height; }));                       //H

            Content = contenedorLogin;
        }

        void AgregarEventos()
        {
            Acceso.Clicked += AccesoApp_Clicked;
            TapOlvidarContrasena.Tapped += TapOlvidarContrasena_Tapped;
        }

        private async void TapOlvidarContrasena_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new OlvidoPassword());
        }

        private async void AccesoApp_Clicked(object sender, EventArgs e)
        {


            Validaciones val = new Validaciones();

            bool ValidarDocumento = val.ValidarDocumento(Documento);
            if (ValidarDocumento)
            {
                await DisplayAlert("Error", "Ingresar Documento.", "Aceptar");
            }
            else
            {
                bool ValidarCodNumerico = val.ValidarCodigoNumerico(Documento);
                if (ValidarCodNumerico)
                {
                    await DisplayAlert("Error", "El documento debe ser numerico", "Aceptar");
                }
                else
                {
                    bool ValidarTamaño = val.ValidarTamañoDocumento(Documento);
                    if (!ValidarTamaño)
                    {
                        await DisplayAlert("Error", "El documento debe tener solo 10 caracteres", "Aceptar");
                    }
                 
                }
            }


            bool ValidarCodigo = val.ValidarCodigo(Codigo);
            if (ValidarCodigo)
            {
                await DisplayAlert("Error", "Ingrese el codigo de estudiante.", "Aceptar");
            }
            else
            {
                bool ValidarNumerico = val.ValidarCodigoNumerico(Codigo);
                if (ValidarNumerico)
                {
                    await DisplayAlert("Error", "El Codigo debe ser numerico.", "Aceptar");
                }
                
              
            }

            bool ValidarContraseña = val.ValidarTextoContraseña(Contraseña);
            if (ValidarContraseña)
            {
                await DisplayAlert("Error", "Ingrese Una Contraseña", "Aceptar");
            }
            else
            {
                bool ValidarCampo = val.ValidarCampoContraseñas(Contraseña);
                if (!ValidarCampo)
                {
                    await DisplayAlert("Error", "La contraseña debe tener solo 7 caracteres", "Aceptar");
                }
                else
                {

                    var respuesta = DependencyService.Get<IRestApi>().CreateClients(Documento.Text, Codigo.Text, Contraseña.Text);
                    if (respuesta.exitoso == 1)
                    {
                        await DisplayAlert("Notificacion", "Bienvenido.", "Aceptar");

                        await Task.Delay(1000);

                        loading.IsVisible = true;
                        await Task.Delay(1000);
                        loading.IsVisible = false;

                        await Navigation.PushAsync(App._masterpage);

                    }
                    else
                    {
                        await DisplayAlert("Notificación", "Error al conectarse al servidor", "Aceptar");
                    }
              
                }
            }

        }
        async Task AnimacionInicial()
        {
            UInt16 Time = 130;

            await Acceso.ScaleTo(0.8, Time); 
            await Acceso.ScaleTo(1, Time);
        }

        async Task AnimacionCirculo()
        {
            UInt16 Time = 1000;

            await circulo.ScaleTo(20, Time);
            await circulo.ScaleTo(0, Time);

            circulo.BackgroundColor = Color.Red;

            await circulo.ScaleTo(20, Time);

            await circulo.FadeTo(0, 700);

            circulo.IsVisible = false;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await AnimacionCirculo();

            await AnimacionInicial();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }
    }
}



