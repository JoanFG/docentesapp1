﻿using DocentesApp;
using DocentesApp.Controlador;
using DocentesApp.Vista.ControlesGenerales;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DocentesApp
{
    public class OlvidoPassword : ContentPage
    {
        Entry entryEmail;
        Button buttonRecoveryPassword;
        StackLayout vistaGeneral, validacion2;
        Label label;
        ActivityIndicator loading;
        Cargando cargando;
        RelativeLayout contenedorGeneral;
        //Image myImage;

        public OlvidoPassword()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            CrearVistas();
            AgregarVistas();
            AgregarEventos();
        }
        void CrearVistas()
        {
            contenedorGeneral = new RelativeLayout();

            
            
            label = new Label
            {
                Text = "Recuperar Password",
                TextColor = Colores.Secundario,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,

            };
            cargando = new Cargando
            {
                IsVisible = false
            };
            loading = new ActivityIndicator
            {
                IsRunning = true,
                IsVisible = false,
                Color = Colores.Primario
            };
            entryEmail = new Entry
            {
                Placeholder = "Email",
                PlaceholderColor = Colores.Primario,
                BackgroundColor = Colores.BarraNavegacion

            };
            buttonRecoveryPassword = new Button
            {
                Text = "Recuperar Password",
                BackgroundColor = Colores.Primario,
                TextColor = Colores.BarraNavegacion,

            };
            vistaGeneral = new StackLayout();
            validacion2 = new StackLayout { Orientation = StackOrientation.Horizontal };
        }


        void AgregarVistas()
        {


            vistaGeneral.Children.Add(entryEmail);
            vistaGeneral.Children.Add(loading);
            validacion2.Children.Add(label);
            vistaGeneral.Children.Add(buttonRecoveryPassword);


            //contenedorGeneral.Children.Add(myImage,
            //     Constraint.RelativeToParent((p) => { return p.Width * 0; }),
            //      Constraint.RelativeToParent((p) => { return p.Height * 0; }),
            //      Constraint.RelativeToParent((p) => { return p.Width * 1; }),
            //      Constraint.RelativeToParent((p) => { return p.Height * 1; }));


            contenedorGeneral.Children.Add(label,
                Constraint.RelativeToParent((p) => { return p.Width * 0; }),
                Constraint.RelativeToParent((p) => { return p.Height * 0.3500; }),
                Constraint.RelativeToParent((p) => { return p.Width * 1; }));

            contenedorGeneral.Children.Add(cargando,
                 Constraint.RelativeToParent((p) => { return p.Width * 0; }),
                 Constraint.RelativeToParent((p) => { return p.Height * 0; }),
                 Constraint.RelativeToParent((p) => { return p.Width * 1; }),
                 Constraint.RelativeToParent((p) => { return p.Height * 1; }));

            contenedorGeneral.Children.Add(vistaGeneral,
                  Constraint.RelativeToParent((p) => { return p.Width * 0.08; }),
                  Constraint.RelativeToParent((p) => { return p.Height * 0.47; }),
                  Constraint.RelativeToParent((p) => { return p.Width * 0.8373; }),
                  Constraint.RelativeToParent((p) => { return p.Height * 0.45; }));


            Content = contenedorGeneral;
        }
        void AgregarEventos()
        {
            buttonRecoveryPassword.Clicked += ButtonRecoveryPassword_Clicked;
        }

        private async void ButtonRecoveryPassword_Clicked(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(entryEmail.Text))
            {
                await this.DisplayAlert("Advertencia", "El campo del correo electronico es obligatorio.", "OK");
            }
            else
            {
                //Valida que el formato del correo sea valido
                bool isEmail = Regex.IsMatch(entryEmail.Text, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);
                if (!isEmail)
                {
                    await this.DisplayAlert("Advertencia", "El formato del correo electrónico es incorrecto, revíselo e intente de nuevo.", "OK");
                }
                else
                {
                    loading.IsVisible = true;
                    await Task.Delay(1000);
                    await DisplayAlert("Notificacion", "Siga las instrucciones en su correo ", "Ok");
                    loading.IsVisible = true;
                    await Task.Delay(1000);
                    await Navigation.PushAsync(new Login());
                }
            }


        }
        //private async void buttonRecoveryPassword_Clicked(object sender, EventArgs e)
        //{
        //    await DisplayAlert("Notificacion", "Siga las instrucciones en su correo ", "Ok");

        //    await Navigation.PushAsync(new Login());

        //    //Validaciones val = new Validaciones();
        //    //if (buttonRecoveryPassword.IsEnabled)
        //    //{
        //    //    //bool respuesta = await DisplayAlert("Notificación", "¿Estas seguro de validar", "Si", "No");
        //    //    //if (respuesta)
        //    //    if (string.IsNullOrEmpty(entryEmail.Text))
        //    //    {
        //    //        await DisplayAlert("Notificacion", "Siga las instrucciones en su correo ", "Ok");
        //    //        //val.ValidarCajasTexto(entryUsuario, entryPassword);
        //    //    }
        //    //    else

        //    //        await Navigation.PushAsync(App._masterPage);
        //    //}
        //    ////loading.IsVisible = false;
        //    //else
        //    //    await Navigation.PushAsync(App._masterPage);
        //    //switch llamar boton (color azul) si esta prendido hacer validaciones, si esta apagado no hacer nada
        //    //.json guardar nota
        //}
    }
}
