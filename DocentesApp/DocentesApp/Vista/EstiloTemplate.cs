﻿using System;
using System.Collections.Generic;
using System.Text;

using Xamarin.Forms;

namespace DocentesApp
{
    public class EstiloTemplate : ViewCell
    {
        public EstiloTemplate()
        {
            StackLayout CeldaPadre = new StackLayout
            {
                Padding = 10,  
                BackgroundColor = Colores.Primario,
                WidthRequest = 375,
                HeightRequest = 70
            };

            StackLayout Contenedor = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.CenterAndExpand,
            };

            Label label = new Label
            {
                VerticalTextAlignment = TextAlignment.Center,
                TextColor = Color.Black,                   
                FontSize = Textos.Items_Lista,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
            };

           
            label.SetBinding(Label.TextProperty, "Titulo");

            Contenedor.Children.Add(label);

            CeldaPadre.Children.Add(Contenedor);

            View = CeldaPadre;
        }
    }
}

