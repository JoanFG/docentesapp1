﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DocentesApp
{
    public static class Colores
    {
        public static Color Login { get; } = Color.FromHex("#2E4F8F");
        public static Color BarraNavegacion { get; } = Color.FromHex("#2E4F8F");
        public static Color Principal { get; } = Color.FromHex("#2E4F8F");
        public static Color Primario { get; } = Color.FromHex("#FFFFFF");
        public static Color Secundario { get; } = Color.FromHex("#000000");

    }
}
