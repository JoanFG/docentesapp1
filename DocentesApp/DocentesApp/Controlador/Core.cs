﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocentesApp.Controlador
{
   public class Core
    {

        public static int ScreenWidth { get; set; }
        public static int ScreenHeight { get; set; }

        public static string nombre_archivo_sesion { get; } = "Sesion_Usuario";


        public static int isLoggedin()
        {
            var session = Storage.getSession();
            if (session== null || string.IsNullOrEmpty(session.Codigo_Docente))
            {
                Console.WriteLine("Not logged in");
                return -1;
            }
            return 1;
        }

    }
}
