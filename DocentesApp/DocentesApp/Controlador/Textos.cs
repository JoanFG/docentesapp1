﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace DocentesApp
{
    public static class Textos
    {
        public static int Bienvenidos { get; } = 36;
        public static int TextoLogin { get; } = 12;
        public static int EnlacesLogin { get; } = 12;
        public static int TextosInformativos { get; } = 14;
        public static int Items_Lista { get; } = 18;
    }
}
