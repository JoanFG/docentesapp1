﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DocentesApp
{
    public class Images
    {
        public static ImageSource LogoUtap { get; } = ImageSource.FromFile("logoutap.png");
        public static ImageSource ImagePlus { get; } = ImageSource.FromFile("ImagePlus.png");
        public static ImageSource IconoAtras { get; } = ImageSource.FromFile("IconoAtras.png");
    }
}
