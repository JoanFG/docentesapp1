﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DocentesApp
{
    public class Validaciones
    {
        //Validacion Numero de Documento
        public bool ValidarDocumento(Entry documento)
        {
            if (string.IsNullOrEmpty(documento.Text))
            {
                return true;
            }
            else
                return false;
        }
        public bool ValidarTamañoDocumento(Entry documento)
        {
            if (documento.Text.Length == 10)
                return true;
            else
                return false;
        }
        public bool ValidarDocNumerico(Entry documento)
        {
            if (string.IsNullOrEmpty(documento.Text))
            {
                return true;
            }
            if (!documento.Text.ToCharArray().All(Char.IsDigit))
            {
                return true;
            }
            else
                return false;
        }


        public bool ValidarCodigo(Entry codigo)
        {
            if (string.IsNullOrEmpty(codigo.Text))
            {
                return true;
            }
            else
                return false;
        }
        public bool ValidarCodigoNumerico(Entry codigo)
        {

            if (!codigo.Text.ToCharArray().All(Char.IsDigit))
            {
                return true;
            }
            else
                return false;
        }


        public bool ValidarTextoContraseña(Entry password)
        {
            if (string.IsNullOrEmpty(password.Text))
            {
                return true;
            }
            else
                return false;
        }


        public bool ValidarCampoContraseñas(Entry password)
        {
            if (password.Text.Length >= 7)
                return true;
            else
                return false;
        }
    }
}
