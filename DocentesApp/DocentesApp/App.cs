﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocentesApp.Controlador;
using DocentesApp.Modelo;
using Xamarin.Forms;

namespace DocentesApp
{
    public class App : Application
    {
        public static MasterPage _masterpage;
        public static Sesion usuario;
        public App()
        {
            if (Core.isLoggedin() == 1)
            {
                usuario = Storage.getSession();

            }
            else
            {
                _masterpage = new MasterPage();

                MainPage = new NavigationPage(new Login()); 
            }
        }
    }
}
