﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocentesApp.Modelo
{
  public  class Sesion
    {
        public string Nombre { get; set; }
        public string Codigo_Docente { get; set; }
        public string Correo_Institucion { get; set; }
    }
}
