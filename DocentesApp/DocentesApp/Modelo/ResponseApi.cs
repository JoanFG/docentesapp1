﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocentesApp.Modelo
{
    public class ResponseAPI
    {
        public string Documento { get; set; }
        public string Codigo { get; set; }
        public string Contraseña { get; set; }
        public int exitoso { get; set; }
        public string mensaje { get; set; }

    }
}
