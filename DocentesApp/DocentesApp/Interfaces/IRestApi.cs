﻿using DocentesApp.Modelo;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocentesApp.Interfaces
{
    public interface IRestApi
    {
        //Se agregan los campos que se envian desde el movil hasta la API
        ResponseAPI CreateClients(string user, string code, string password);

    }
}