﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DocentesApp.Droid.Servicios;
using DocentesApp.Interfaces;
using DocentesApp.Modelo;
using Newtonsoft.Json;
using RestSharp;
using Xamarin.Forms;


[assembly: Dependency(typeof(Api))]
namespace DocentesApp.Droid.Servicios
{


    public class Api : IRestApi
    {
        // LOS CAMPOS QUE ENVIAMOS A LA API
        public ResponseAPI CreateClients(string user, string code, string password)
        {
            try
            {
                //CONEXION CON LA API 
                RestClient _Client = new RestClient("http://todoapijfg.azurewebsites.net/api/login/authenticate");
                RestRequest _request = new RestRequest("", Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };

                //_request.AddBody();

                _request.AddParameter("Documento", user);
                _request.AddParameter("Codigo", code);
                _request.AddParameter("Contraseña", password);

                var respuesta = _Client.Execute(_request);
                Console.WriteLine("The Server return: " + respuesta.Content);

                var responseAPI = JsonConvert.DeserializeObject<ResponseAPI>(respuesta.Content);
                return responseAPI;


            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
        }
    }
}